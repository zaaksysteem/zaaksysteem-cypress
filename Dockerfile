FROM cypress/browsers:chrome67

WORKDIR /usr/app

COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm i

COPY cypress.json cypress.json
COPY cypress cypress

CMD npm run start
