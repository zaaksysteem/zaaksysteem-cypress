# Cypress
Cypress is used for E2E testing

## Running interactive
Because Cypress is an interactive tool that needs access to your Browser you cannot
run this using Docker. 

### Installing
Run `npm install` in the `test` directory

### Start
Run `npm run cypress` in the `test` directory. You will be prompted to chose the environment
you want to test and to input the username/password for this environment. See `Saving your credentials`
chapter to skip username/password input

### Saving your credentials
To skip inputting your username/password for environments you can create a `credentials.json` file
in the `test` directory.

The contents of this file should look like this:

```
{
  "local": {
    "username": "secret",
    "password": "secret",
  },
  "development": {
    "username": "secret",
    "password": "secret",
  },
  "hotfix": {
    "username": "secret",
    "password": "secret",
  }
}
```