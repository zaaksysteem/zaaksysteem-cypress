const inquirer = require('inquirer');
const { exec } = require('child_process');
const { promisify } = require('util');

const globAsync = promisify(require('glob'));
const readFileAsync = promisify(require('fs').readFile);

const environments = [
  { name: 'local', url: 'https://dev.zaaksysteem.nl' },
  { name: 'development', url: 'https://development.zaaksysteem.nl' },
  { name: 'hotfix', url: 'https://hotfix.zaaksysteem.nl' },
];

const readTagsFromFile = async file => {
  const data = await readFileAsync(file, 'utf8');
  return data.match(/\@\S*/gm);
};

const searchAllTags = async () => {
  const files = await globAsync('cypress/integration/**/*.feature');
  const tagsFromFiles = await Promise.all(files.map(readTagsFromFile));

  return [...new Set([].concat(...tagsFromFiles))];
};

const getCredentialsFromDisk = async () => {
  try {
    const data = await readFileAsync('credentials.json', 'utf8');
    return JSON.parse(data);
  } catch (e) {
    return {};
  }
};

const promptEnviroment = async () => {
  const { environment } = await inquirer.prompt({
    type: 'list',
    name: 'environment',
    message: 'Which environment would you like to test?',
    choices: environments.map(value => value.name),
  });

  return environments.find(item => item.name === environment);
};

const promptCredentials = async () =>
  inquirer.prompt([
    {
      type: 'input',
      name: 'username',
      message: 'Username',
      default: 'admin',
    },
    {
      type: 'input',
      name: 'password',
      message: 'Password',
      default: 'admin',
    },
  ]);

const promptTags = async () => {
  const { runTags } = await inquirer.prompt([
    {
      type: 'confirm',
      name: 'runTags',
      message: 'Would you like to run specific tags?',
      default: false,
    },
  ]);

  if (runTags) {
    const tagsFromFeatures = await searchAllTags();
    const { tags } = await inquirer.prompt([
      {
        type: 'checkbox',
        name: 'tags',
        message: 'Select tags below',
        choices: tagsFromFeatures,
      },
    ]);

    return tags.join(' or ');
  }

  return null;
};

const getUserInput = async () => {
  const credentials = await getCredentialsFromDisk();
  const environment = await promptEnviroment();
  const tags = await promptTags();
  const environmentCredentials = credentials[environment.name];
  const { username, password } =
    typeof environmentCredentials !== 'undefined'
      ? environmentCredentials
      : await promptCredentials();

  return {
    url: environment.url,
    username,
    password,
    tags,
  };
};

const startCypress = ({ username, password, url, tags }) =>
  exec(
    `CYPRESS_baseUrl=${url} CYPRESS_username=${username} CYPRESS_password=${password} npm run cypress:interactive ${
      tags ? `-- -e TAGS="${tags}"` : ''
    }`
  );

const start = async () => {
  const options = await getUserInput();
  startCypress(options);
};

start();
