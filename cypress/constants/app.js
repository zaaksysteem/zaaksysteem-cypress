export const DRAWER_MENU_TOGGLE = 'admin:layout:app-bar:menu-toggle:button';
export const DRAWER_LINK_CATALOGUS =
  'admin:layout:drawer:primary:menu:catalogus:item';
export const DRAWER_LINK_GEBRUIKERS =
  'admin:layout:drawer:primary:menu:gebruikers:item';
export const DRAWER_LINK_LOG = 'admin:layout:drawer:primary:menu:logboek:item';
export const DRAWER_LINK_TRANSACTIES =
  'admin:layout:drawer:primary:menu:transacties:item';
export const DRAWER_LINK_KOPPELINGEN =
  'admin:layout:drawer:primary:menu:koppelingen:item';
export const DRAWER_LINK_GEGEVENS =
  'admin:layout:drawer:primary:menu:gegevens:item';
export const DRAWER_LINK_CONFIGURATIE =
  'admin:layout:drawer:primary:menu:configuratie:item';
export const DRAWER_LINK_BEHANDELEN =
  'admin:layout:drawer:secondary:menu:behandelen:item';
export const DRAWER_LINK_HELP = 'admin:layout:drawer:secondary:menu:help:item';

export const USER_MENU_TOGGLE = 'admin:layout:app-bar:user-menu:button';
export const USER_MENU_LOGOUT =
  'admin:layout:app-bar:user-menu:uitloggen:button';
