export const selectValueFromReactSelect = (selector, value) => {
  cy.get(`${selector} .react-select__control`).click();
  cy.get(`${selector} .react-select__input input`).type(value, {
    force: true,
  });
  cy.get(`${selector} .react-select__input input`).type('{enter}');
};
