Feature:  Catalog

  I want to test the Catalog features

  @catalog @catalog-export
  Scenario: Exporting a case type
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "generic" folder
    And I select a "case_type" with the name "Casetype generic"
    And I press the "Exporteren" button in the advanced menu
    Then the casetype gets downloaded

  @catalog @catalog-edit
  Scenario: Edit a case type
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "generic" folder
    And I click on the "case_type" with the name "Casetype generic"
    Then I get redirected to the old page
    When I finish editing
    Then I get redirected back to the catalog

  @catalog @catalog-edit
  Scenario: Edit a case type (with edit button)
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "generic" folder
    And I select a "case_type" with the name "Casetype generic"
    And I press the "edit" button in the button bar
    Then I get redirected to the old page
    When I finish editing
    Then I get redirected back to the catalog

  @catalog @catalog-copy
  Scenario: Copy a case
    Given I'm on the Catalog page
    When I navigate to the right directory
    And I select a "case_type" with the name "zaaktypeerman2"
    And I validate I have one specific file
    And I copy the file
    Then  I have copied the file

  @catalog @catalog-online-offline
  Scenario: Taking a case type offline
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "online/offline" folder
    And I select a case type that is online
    And I press the "Offline zetten" button in the advanced menu
    Then I’m presented with a dialog
    When I provide the reason "Pretty much a valid reason"
    And I press the "Offline zetten" button in the dialog
    Then The dialog closes
    And The case type with the name "000 zaaktype" is now "offline"

  @catalog @catalog-online-offline
  Scenario: Taking a case type online
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "online/offline" folder
    And I select a case type that is offline
    And I press the "Online zetten" button in the advanced menu
    Then I’m presented with a dialog
    When I provide the reason "Pretty much a valid reason"
    And I press the "Online zetten" button in the dialog
    Then The dialog closes
    And The case type with the name "000 zaaktype" is now "online"

  @catalog @catalog-online-offline
  Scenario: Taking a case type offline, but cancel action
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "online/offline" folder
    And I select a "case_type" with the name "Casetype"
    And I press the "Offline zetten" button in the advanced menu
    Then I’m presented with a dialog
    And I press the "Annuleren" button in the dialog
    Then The dialog closes

  @catalog @catalog-move
  Scenario: Moving items to a different folder
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "moving" folder
    And I navigate to the "a" folder
    And I select the first item | tag="movableItemName"
    And I press the "move" button in the button bar
    Then I'm presented with a move bar
    When I click on the breadcrumb with the name "moving"
    And I navigate to the "b" folder
    And I interact with the move bar by pressing the "move" button
    Then The item should be in the current folder | tag="movableItemName"

  @catalog @catalog-move
  Scenario: Moving items back to original folder
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "moving" folder
    And I navigate to the "b" folder
    And I select the first item | tag="movableItemName"
    And I press the "move" button in the button bar
    Then I'm presented with a move bar
    When I click on the breadcrumb with the name "moving"
    And I navigate to the "a" folder
    And I interact with the move bar by pressing the "move" button
    Then The item should be in the current folder | tag="movableItemName"

  @catalog @catalog-move
  Scenario: Cancelling a move
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "moving" folder
    And I navigate to the "a" folder
    And I select the first item | tag="movableItemName"
    And I press the "move" button in the button bar
    Then I'm presented with a move bar
    When I click on the breadcrumb with the name "moving"
    And I navigate to the "a" folder
    And I interact with the move bar by pressing the "cancel" button
    Then The item should be in the current folder | tag="movableItemName"

  @catalog @catalog-move
  Scenario: Moving a folder in itself
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "moving" folder
    And I navigate to the "folder" folder
    And I select a "folder" with the name "child"
    And I press the "move" button in the button bar
    Then I cannot navigate into the folder with the name "child"

  Scenario:  Creating a casetype via the plusbutton
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Create Casetype" folder
    And I click on the plusbutton
    And I click on the button with the name 'case_type' in the plusbutton
    And I create a casetype via the plusbutton | tag="Plusbuttoncreatecasetype"
    Then The item should be in the current folder | tag="Plusbuttoncreatecasetype"

  Scenario: Creating an element of the type "objecttype" via the "plusbutton"
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Create Object" folder
    And I click on the plusbutton
    And I click on the button with the name 'object_type' in the plusbutton
    And I create an object via the plusbutton | tag="Plusbuttoncreateobject"
    Then The item should be in the current folder | tag="Plusbuttoncreateobject"

  Scenario: Editing an element of the type "objecttype"
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Edit Object" folder
    And I select a "object_type" with the name "EditObject"
    And I press the "edit" button in the button bar
    And I edit an object | tag="EditObject"
    Then The item should be in the current folder | tag="EditObject"

  Scenario: Create an attribute
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Create Attribute" folder
    And I click on the plusbutton
    And I click on the button with the name 'attribute' in the plusbutton
    And I create an attribute | tag="createattribute"
    Then The item should be in the current folder | tag="createattribute"

  Scenario: Edit an attribute
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Create Attribute" folder
    And I select a "name-wrapper" with the name "createattribute"
    And I press the "edit" button in the button bar
    And I edit an attribute | tag="editfolder"
    Then The item should be in the current folder | tag="editfolder"

  Scenario: Create an emailtemplate
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Create Emailtemplate" folder
    And I click on the plusbutton
    And I click on the button with the name 'email_template' in the plusbutton
    And I create a template | tag="createemailtemplate"
    Then The item should be in the current folder | tag="createemailtemplate"

  Scenario: Edit an emailtemplate
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Create Emailtemplate" folder
    And I select a "name-wrapper" with the name "createemailtemplate"
    And I press the "edit" button in the button bar
    And I edit a template | tag="editemailtemplate"
    Then The item should be in the current folder | tag="editemailtemplate"

  Scenario: Open elementdialog via the plusbutton
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I click on the plusbutton
    Then the elementdialog is opened correctly

  Scenario: Close the elementdialog
    Given I'm on the Catalog page
    When I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I click on the plusbutton
    And I close the elementdialog
    Then the elementdialog is closed correctly

  Scenario: Create an documenttemplate
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Create Documenttemplate" folder
    And I click on the plusbutton
    And I click on the button with the name 'document_template' in the plusbutton
    And I create a documenttemplate | tag="createdocumenttemplate"
    Then The item should be in the current folder | tag="createdocumenttemplate"

  Scenario: Edit an documenttemplate
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Edit Documenttemplate" folder
    And I select a "name-wrapper" with the name "editdocumenttemplate"
    And I press the "edit" button in the button bar
    And I edit a documenttemplate | tag="editemailtemplate"
    Then The item should be in the current folder | tag="editemailtemplate"

  Scenario: Search for a keyword
    Given I'm on the Catalog page
    And I search for a query "test"
    Then I see my searchresults for the query "test" in the correct screen

  Scenario: Closing the searching
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Edit Documenttemplate" folder
    And I search for a query 'test'
    And I close the searching
    Then the searching is closed correctly
    And I am in the "Edit Documenttemplate" folder


  Scenario: View previous versions of a casetype in the catalog
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Version" folder
    And I select a "case_type" with the name "ermantest55"
    And I press the "history" button in the button bar
    Then I see the previous versions of my casetype

  Scenario: Switch to previous versions of a casetype in the catalog
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Version" folder
    And I select a "case_type" with the name "ermantest55"
    And I press the "history" button in the button bar
    And I activate a different version of my casetype
    Then I am in the "Version" folder

  Scenario: Adding and Removing an element from the catalog
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Edit Folder" folder
    And I click on the plusbutton
    And I click on the button with the name 'folder' in the plusbutton
    And I create a folder | tag="createfolder"
    And I select a "folder" with the | tag="createfolder"
    And I press the "delete" button in the button bar
    And I delete the element
    Then the "folder" with the name "createfolder" has been removed succesfully

  Scenario: Edit a folder
    Given I'm on the Catalog page
    When  I navigate to the "Cypress" folder
    And I navigate to the "Plusbutton" folder
    And I navigate to the "Create Folder" folder
    And I select a "folder" with the name "createfolder"
    And I press the "edit" button in the button bar
    And I edit a folder | tag="editfolder"
    Then The item should be in the current folder | tag="editfolder"