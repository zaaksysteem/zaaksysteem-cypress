Feature: Communication

    Scenario: Go to the Communication-tab in my case
        Given I navigate to my case
        When I navigate to the communication tab
        Then I am in the communication tab

    Scenario: Create a contactmoment
        Given I navigate to my case
        When I navigate to the communication tab
        And I create a contactmoment | tag="newcontactmoment"
        Then I see my created contactmoment | tag="newcontactmoment"

    Scenario: Create a note
        Given I navigate to my case
        When I navigate to the communication tab
        And I create a note | tag="newnote"
        Then I see my created note | tag="newnote"

    Scenario: Create a message
        Given I navigate to my case
        When I navigate to the communication tab
        And I create a message | tag="newmessage"
        Then I see my created message | tag="newmessage"

    Scenario:
        Given I login
        When I navigate to the communication tab from the dashboard
        Then I am in the communication tab from the dashboard