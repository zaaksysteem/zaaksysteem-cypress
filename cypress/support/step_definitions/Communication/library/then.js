import { Then } from 'cypress-cucumber-preprocessor/steps';

Then('I see my created contactmoment | tag={string}', id => {
  cy.wait(400);
  cy.get(`@${id}`).then(value => {
    cy.contains(value)
      .scrollIntoView()
      .should('be.visible');
  });
});

Then('I am in the communication tab', () => {
  cy.getScoped(`add-thread:button`).should('be.visible');
});

Then('I see my created note | tag={string}', id => {
  cy.wait(400);
  cy.get(`@${id}`).then(value => {
    cy.contains(value)
      .scrollIntoView()
      .should('be.visible');
  });
});

Then('I see my created message | tag={string}', id => {
  cy.wait(400);
  cy.get(`@${id}`).then(value => {
    cy.contains(value)
      .scrollIntoView()
      .should('be.visible');
  });
});

Then('I am in the communication tab from the dashboard', () => {
  cy.contains('Selecteer een item om te lezen').should('be.visible');
});
