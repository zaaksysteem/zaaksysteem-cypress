import { Given } from 'cypress-cucumber-preprocessor/steps';
import { DRAWER_LINK_CATALOGUS } from '../../../../constants/app';

Given(`I navigate to my case`, () => {
  cy.login();
  cy.viewport(1500, 800);
  cy.visit('https://development.zaaksysteem.nl/intern/zaak/10540');
});

Given(`I login`, () => {
  cy.login();
  cy.visit('https://development.zaaksysteem.nl/intern/');
});
