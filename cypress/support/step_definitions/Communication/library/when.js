import { selectValueFromReactSelect } from '../../../../library/select';
import { When } from 'cypress-cucumber-preprocessor/steps';

When('I create a contactmoment | tag={string}', id => {
  cy.viewport(1500, 800);
  cy.wait(400);
  cy.getScoped('add-thread:button').click();

  const name = 'test' + Date.now();
  cy.getScoped('contact-moment-component-content:text-field-input').type(name);
  cy.wrap(name).as(id);

  cy.get('button')
    .contains('Toevoegen')
    .click();
});

When('I navigate to the communication tab', () => {
  cy.viewport(1500, 800);
  cy.wait(400);
  cy.get('a[data-name=communication]').click();
});

When('I create a note | tag={string}', id => {
  cy.viewport(1500, 800);
  cy.wait(400);
  cy.getScoped('add-thread:button').click();

  cy.getScoped('add-form:tab:contactmoment')
    .contains('Notitie')
    .click();

  const name = 'testnote' + Date.now();
  cy.getScoped('note-component-content:text-field-input').type(name);
  cy.wrap(name).as(id);

  cy.get('button')
    .contains('Toevoegen')
    .click();
});

When('I create a message | tag={string}', id => {
  cy.viewport(1500, 600);
  cy.wait(400);

  cy.getScoped('add-thread:button').click();

  cy.getScoped('add-form:tab:message')
    .contains('Bericht')
    .click();

  cy.getScoped('note-component-subject:text-field-input').type(
    'onderwerp message'
  );

  const name = 'testmessage' + Date.now();
  cy.getScoped('note-component-content:text-field-input').type(name);

  cy.wrap(name).as(id);

  cy.get('button')
    .contains('Toevoegen')
    .click();
});

When('I navigate to the communication tab from the dashboard', () => {
  cy.viewport(1500, 800);
  cy.get('.top-bar-menu-button').click();
  cy.contains('Communicatie').click();
});
