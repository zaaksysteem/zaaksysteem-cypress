import { Then } from 'cypress-cucumber-preprocessor/steps';

Then('the casetype gets downloaded', () => {
  cy.get('@locationAssignStub').should('be.called');
});

Then('I get redirected to the old page', () => {
  cy.url().should('include', 'return_url');
  cy.get('iframe').should('have.length', 1);
});

Then('I get redirected back to the catalog', () => {
  cy.get('iframe').should('have.length', 0);
  cy.url().should('include', '/admin/catalog');
});

Then('I have copied the file', () => {
  cy.get('@duplicatedCaseType').then(value => {
    cy.getScoped('catalog-item:case_type:link')
      .contains(value)
      .should('have.length', 1);
  });
});

Then('I have copied the file', () => {
  cy.get('@duplicatedCaseType').then(value => {
    cy.getScoped('catalog-item:case_type:link')
      .contains(value)
      .should('have.length', 1);
  });
});

Then('I’m presented with a dialog', () => {
  cy.getScoped('catalog:changeactive:text-field-input').should('be.visible');
});

Then('The dialog closes', () => {
  cy.getScoped('catalog:changeactive:text-field-input').should(
    'not.be.visible'
  );
});

Then('The case type with the name {string} is now {string}', (name, status) => {
  switch (status.toLowerCase()) {
    case 'online':
      return cy
        .getScoped(`catalog-item:case_type:link`)
        .should('not.contain', 'Offline');

    case 'offline':
      return cy
        .getScoped(`catalog-item:case_type:link`)
        .siblings('span')
        .should('contain', 'Offline');

    default:
      return cy.log(`Unknown status for case type: ${status}`);
  }
});

Then(`I'm presented with a move bar`, () => {
  cy.getScoped('catalog:move-bar').should('be.visible');
});

Then('The item should be in the current folder | tag={string}', id => {
  cy.get(`@${id}`).then(value => {
    cy.contains(value)
      .scrollIntoView()
      .should('be.visible');
  });
});

Then('I should be redirected in the created folder | tag={string}', id => {
  cy.get(`@${id}`).then(value => {
    cy.contains(value)
      .scrollIntoView()
      .should('be.visible');
  });
});

Then('I cannot navigate into the folder with the name {string}', name => {
  cy.contains(name)
    .parent()
    .children('[data-scope="catalog-item:folder:link"]')
    .should('not.exist');
});

Then('the elementdialog is opened correctly', () => {
  cy.contains('Element toevoegen').should('be.visible');
});

Then('the elementdialog is closed correctly', () => {
  cy.contains('Element toevoegen').should('not.be.visible');
});

Then(
  'I see my searchresults for the query {string} in the correct screen',
  name => {
    cy.contains('Zoekresultaten').should('be.visible');

    cy.contains(name);
  }
);

Then('the searching is closed correctly', () => {
  cy.contains('Zoekresultaten').should('not.be.visible');
});

Then('I am in the {string} folder', name => {
  cy.contains(name);
});

Then('I see the previous versions of my casetype', () => {
  cy.contains('Versiebeheer').should('be.visible');
});

Then(
  'the {string} with the name {string} has been removed succesfully',
  (type, name) => {
    cy.getScoped(`catalog-item:${type}:link`)
      .contains(name)
      .should('not.be.visible');
  }
);