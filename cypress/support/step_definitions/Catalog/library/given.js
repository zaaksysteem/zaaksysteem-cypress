import { Given } from 'cypress-cucumber-preprocessor/steps';
import { DRAWER_LINK_CATALOGUS } from '../../../../constants/app';

Given(`I'm on the Catalog page`, () => {
  cy.login();
  cy.drawerNavigate(DRAWER_LINK_CATALOGUS);

  // Stub location assign
  cy.window().then(win => {
    win.locationAssign = () => {};
    cy.stub(win, 'locationAssign').as('locationAssignStub');
  });

  //Start Cypress server so we can monitor xhr requests
  cy.server();

  cy.route('get', '**/get_folder_contents*').as('getFolderContents');
});
