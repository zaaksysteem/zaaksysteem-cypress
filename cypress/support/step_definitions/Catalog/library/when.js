import { selectValueFromReactSelect } from '../../../../library/select';
import { When } from 'cypress-cucumber-preprocessor/steps';

const selectOtherElementInTheSameRow = (element, target) => {
  return element.parents('[role="row"]').getScoped(target);
};

When('It contains a casetype', () => {
  cy.server();
  cy.fixture('catalog-fixture.json').as('catalog');
  cy.route('GET', 'get_folder_contents', '@catalog').as('get_folder_contents');
  cy.reload();
});

When('I navigate to the {string} folder', name => {
  cy.getScoped(`catalog-item:folder:link`)
    .contains(name)
    .scrollIntoView()
    .click();

  cy.wait('@getFolderContents');
  cy.getScoped('catalog:breadcrumbs:item-last').should('contain', name);
});

When('I click on the {string} with the name {string}', (type, name) => {
  cy.getScoped(`catalog-item:${type}:link`)
    .contains(name)
    .click();
});

When('I select a {string} with the name {string}', (type, name) =>
  selectOtherElementInTheSameRow(
    cy.getScoped(`catalog-item:${type}`).contains(name),
    'checkbox'
  )
    .first()
    .click()
);

When('I select the first item | tag={string}', id => {
  cy.getScoped('checkbox')
    .first()
    .click();

  selectOtherElementInTheSameRow(cy.getScoped('checkbox').first(), 'link')
    .invoke('text')
    .then(value => {
      cy.log(value);
      cy.wrap(value).as(id);
    });
});

When('I click on the breadcrumb with the name {string}', name => {
  cy.getScoped(`catalog:breadcrumbs`)
    .contains(name)
    .click();

  cy.wait('@getFolderContents');
  cy.getScoped('catalog:breadcrumbs:item-last').should('contain', name);
});

When('I press the {string} button in the advanced menu', button => {
  const normalizedButton = button
    .toLowerCase()
    .split(' ')
    .join('-');

  cy.getScoped('catalog-header:button-bar:advanced:button').click();
  cy.getScoped(`catalog-header:button-bar:${normalizedButton}:button`).click();
});

When('I press the {string} button in the button bar', button => {
  const map = {
    edit: 'edit',
    move: 'folder_move',
    history: 'history',
    delete: 'delete',
  };
  cy.getScoped(`catalog-header:button-bar:${map[button]}:button`).click();
});

When('I select a case type that is online', () => {
  selectOtherElementInTheSameRow(
    cy.getScoped(`catalog-item:case_type:link`).contains(/.+? (?!Offline)/g),
    'checkbox'
  )
    .first()
    .click();
});

When('I select a case type that is offline', () => {
  selectOtherElementInTheSameRow(
    cy
      .getScoped(`catalog-item:case_type:link`)
      .siblings('span')
      .contains('Offline'),
    'checkbox'
  )
    .first()
    .click();
});

When('I finish editing', () => {
  cy.get('iframe')
    .onload()
    .iframe()
    .find('.ezra_zaaktype_goto')
    .contains('afronden')
    .click();

  cy.get('iframe')
    .onload()
    .iframe()
    .find('#basisattributen')
    .click();

  cy.get('iframe')
    .iframe()
    .find('input[type="submit"]')
    .contains('Publiceren')
    .click();
});

When('I provide the reason {string}', reason => {
  cy.getScoped('catalog:changeactive:text-field-input').type(reason);
});

When('I press the {string} button in the dialog', action => {
  const normalizedAction = action
    .toLowerCase()
    .split(' ')
    .join('-');

  cy.getScoped(
    `catalog:changeactive:dialog:action:${normalizedAction}:button`
  ).click();
});

When('I navigate to the right directory', () => {
  cy.visit(
    'https://development.zaaksysteem.nl/admin/catalogus/79ecfc53-c617-44ae-aa64-c8fd9ab40c17'
  );
});

When('I validate I have one specific file', () => {
  cy.getScoped('catalog-item:case_type:link')
    .contains('zaaktypeerman2')
    .should('have.length', 1);
});

When('I copy the file', () => {
  const duplicatedCaseType = `duplicated casetype ${Date.now()}`;
  cy.wrap(duplicatedCaseType).as('duplicatedCaseType');

  cy.getScoped('catalog-header:button-bar:file_copy:button').click();

  cy.get('iframe')
    .onload()
    .iframe()
    .find('input[name="node.titel"]')
    .clear()
    .type(duplicatedCaseType);

  cy.get('iframe')
    .iframe()
    .find('.ezra_zaaktype_goto')
    .contains('afronden')
    .click();

  cy.get('iframe')
    .onload()
    .iframe()
    .find('#basisattributen')
    .click();

  cy.get('iframe')
    .iframe()
    .find('input[type="submit"]')
    .contains('Publiceren')
    .click();
});

When('I interact with the move bar by pressing the {string} button', action => {
  cy.getScoped(`catalog:move-bar:${action}:button`).click();
});

When('I click on the button with the name {string} in the plusbutton', name => {
  const normalizedName = name
    .toLowerCase()
    .split(' ')
    .join('-');

  cy.getScoped(`catalog:add-element:button:${normalizedName}`).click();
});

When('I create a casetype via the plusbutton | tag={string}', id => {
  const name = 'plusbuttoncasetypetest' + Date.now();
  cy.get('iframe')
    .onload()
    .iframe()
    .find('[name="node.titel"]')
    .type(name);
  cy.wrap(name).as(id);

  cy.get('iframe')
    .iframe()
    .find('[name="zaaktype.bibliotheek_categorie_id"]');

  cy.get('iframe')
    .iframe()
    .find('[name="node.code"]')
    .type('1');

  cy.get('iframe')
    .iframe()
    .find('[name="definitie.grondslag"]')
    .type('1');

  cy.get('iframe')
    .iframe()
    .find('[name="definitie.afhandeltermijn"]')
    .type('1');

  cy.get('iframe')
    .iframe()
    .find('[name="definitie.servicenorm"]')
    .type('1');

  cy.get('iframe')
    .iframe()
    .find('.ezra_zaaktype_goto')
    .contains('afronden')
    .click();

  cy.get('iframe')
    .onload()
    .iframe()
    .find('#basisattributen')
    .click();

  cy.get('iframe')
    .iframe()
    .find('input[type="submit"]')
    .contains('Publiceren')
    .click();
});

When('I create an object via the plusbutton | tag={string}', id => {
  const name = 'Plusbuttoncreateobject' + Date.now();
  cy.get('iframe')
    .onload()
    .iframe()
    .find('[name="name"]')
    .type(name);
  cy.wrap(name).as(id);

  cy.get('iframe')
    .iframe()
    .find('[name="title_template"]')
    .type('objectittel');

  cy.get('iframe')
    .iframe()
    .find('[name="category_id"]')
    .select('-- Create Object');

  cy.get('iframe')
    .iframe()
    .find('button')
    .contains('Volgende')
    .click();

  cy.get('iframe')
    .iframe()
    .find('button')
    .contains('Volgende')
    .click();

  cy.get('iframe')
    .iframe()
    .find('button')
    .contains('Volgende')
    .click();

  cy.get('iframe')
    .iframe()
    .find('[name="modification_rationale"]')
    .type('objecttest');

  cy.get('iframe')
    .iframe()
    .find('[name="modified_sections"] input[type="checkbox"]')
    .click({ multiple: true });

  cy.get('iframe')
    .iframe()
    .find('button.button-primary')
    .contains('Afronden')
    .click();

  cy.visit(
    'https://development.zaaksysteem.nl/admin/catalogus/f02ca814-ea58-42b6-acdb-93d28a5fa1c2'
  );
});

When('I edit an object | tag={string}', id => {
  const name = 'EditObject' + Date.now();
  cy.get('iframe')
    .onload()
    .iframe()
    .find('[name="name"]')
    .clear()
    .type(name);
  cy.wrap(name).as(id);

  cy.get('iframe')
    .iframe()
    .find('button')
    .contains('Volgende')
    .click();

  cy.get('iframe')
    .iframe()
    .find('button')
    .contains('Volgende')
    .click();

  cy.get('iframe')
    .iframe()
    .find('button')
    .contains('Volgende')
    .click();

  cy.get('iframe')
    .iframe()
    .find('[name="modification_rationale"]')
    .type('editobject');

  cy.get('iframe')
    .iframe()
    .find('[name="modified_sections"] input[type="checkbox"]')
    .click({ multiple: true });

  cy.get('iframe')
    .iframe()
    .find('button.button-primary')
    .contains('Opslaan')
    .click();

  cy.visit(
    'https://development.zaaksysteem.nl/admin/catalogus/2e95e301-0f63-416b-ad46-32cc5c5da32a'
  );
});

When('I create a folder | tag={string}', id => {
  const name = 'createfolder' + Date.now();

  cy.getScoped('folder-form-component-name:text-field-input').type(name);

  cy.wrap(name).as(id);

  cy.getScoped('catalog-folder-dialog:opslaan:opslaan:button').click();
});

When('I edit a folder | tag={string}', id => {
  const name = 'editfolder' + Date.now();

  cy.getScoped('folder-form-component-name:text-field-input').clear();

  cy.getScoped('folder-form-component-name:text-field-input').type(name);

  cy.wrap(name).as(id);

  cy.getScoped('catalog-folder-dialog:opslaan:opslaan:button').click();
});

When('I create an attribute | tag={string}', id => {
  const name = 'createattribute' + Date.now();
  cy.getScoped('attribute-form-component-name:text-field-input').type(name);
  cy.wrap(name).as(id);

  cy.getScoped(
    'attribute-form-component-magic_string:text-field-input'
  ).click();

  cy.get('#attribute_type').scrollIntoView();
  selectValueFromReactSelect('#attribute_type', 'Straat');

  cy.getScoped('catalog-attribute-dialog:opslaan:opslaan:button').click();
});

When('I edit an attribute | tag={string}', id => {
  const name = 'editfolder' + Date.now();

  cy.getScoped('attribute-form-component-name:text-field-input').clear();

  cy.getScoped('attribute-form-component-name:text-field-input').type(name);

  cy.wrap(name).as(id);

  cy.getScoped('catalog-attribute-dialog:opslaan:opslaan:button').click();
});

When('I create a template | tag={string}', id => {
  const name = 'createemailtemplate' + Date.now();
  cy.getScoped('email-template-form-component-label:text-field-input').type(
    name
  );
  cy.wrap(name).as(id);

  cy.getScoped('email-template-form-component-subject:text-field-input')
    .click()
    .type('testonderwerp');

  cy.getScoped('email-template-form-component-message:text-field-input')
    .click()
    .type('testbericht');

  cy.getScoped('catalog-email-template-dialog:opslaan:opslaan:button').click();
});

When('I edit a template | tag={string}', id => {
  const name = 'editemailtemplate' + Date.now();

  cy.getScoped('email-template-form-component-label:text-field-input').clear();

  cy.getScoped('email-template-form-component-label:text-field-input').type(
    name
  );
  cy.wrap(name).as(id);

  cy.getScoped('catalog-email-template-dialog:opslaan:opslaan:button').click();
});

When('I click on the plusbutton', name => {
  cy.getScoped(`catalog-actions:button`).click();
});

When('I close the elementdialog', name => {
  cy.getScoped(`catalog-add-element-dialog:close:button`).click();
});

When('I create a documenttemplate | tag={string}', id => {
  const name = 'createdocumenttemplate' + Date.now();

  cy.getScoped('document-template-form-component-name:text-field-input').type(
    name
  );

  cy.wrap(name).as(id);

  cy.get('#integration_uuid').scrollIntoView();
  selectValueFromReactSelect('#integration_uuid', 'StUF-DCR');

  cy.getScoped(
    'document-template-form-component-integration_reference:text-field-input'
  ).type('test');

  cy.getScoped(
    'document-template-form-component-commit_message:text-field-input'
  )
    .clear()
    .type('test');

  cy.getScoped(
    'catalog-document-template-dialog:opslaan:opslaan:button'
  ).click();
});

When('I edit a documenttemplate | tag={string}', id => {
  const name = 'editdocumenttemplate' + Date.now();

  cy.getScoped('document-template-form-component-name:text-field-input')
    .clear()
    .type(name);

  cy.wrap(name).as(id);

  cy.getScoped(
    'catalog-document-template-dialog:opslaan:opslaan:button'
  ).click();
});

When('I search for a query {string}', name => {
  name
    .toLowerCase()
    .split(' ')
    .join('-');

  cy.getScoped('catalog-search:text-field-input')
    .type(name)
    .type('{enter}');
});

When('I close the searching', name => {
  cy.getScoped(`catalog-search:close-button`).click();
});

When('I activate a different version of my casetype', () => {
  cy.getScoped(`catalog-header:button-bar:activate:button`)

    .first()
    .click({});

  cy.getScoped(
    'case-type-versions-activate-form-component-reason:text-field-input'
  ).type('automationtest versioning');

  cy.getScoped('catalog-folder-dialog:opslaan:opslaan:button').click();
});

When('I delete the element', () => {
  cy.getScoped(`catalog-delete-dialog:component:reason:text-field-input`).type(
    'End To End Test - Delete'
  );

  cy.getScoped('catalog-folder-dialog:verwijderen:verwijderen:button').click();
});

When('I select a {string} with the | tag={string}', (type, id) =>
  cy.get(`@${id}`).then(tag => {
    selectOtherElementInTheSameRow(
      cy.getScoped(`catalog-item:${type}`).contains(tag),
      'checkbox'
    )
      .scrollIntoView()
      .click();
  })
);