import fetch from 'unfetch';
import './commands';

// Overwrite fetch API with XHR api, since Cypress cannot monitor Fetch requests
Cypress.on('window:before:load', win => {
  win.fetch = undefined;
  win.eval(`window.fetch=${fetch}`);
});
